BEGIN PLOT /BELLE_2022_I214037
LogY=0
END PLOT

BEGIN PLOT /BELLE_2022_I2140379/d01-x01-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^+\pi^0$ and $\bar\Lambda_c^-\to\bar\Sigma^-\pi^0$
YLabel=$\alpha_{\Sigma^+}\times\alpha_{\Lambda_c\to\Sigma^+\pi^0}$
YMin=-1.
YMax=1.
END PLOT
BEGIN PLOT /BELLE_2022_I2140379/d01-x01-y02
Title=$\alpha_{\Lambda_c\to\Sigma^+\pi^0}$ for $\Lambda_c^+\to\Sigma^+\pi^0$ and $\bar\Lambda_c^-\to\bar\Sigma^-\pi^0$
YLabel=$\alpha_{\Lambda_c\to\Sigma^+\pi^0}$
YMin=-1.
YMax=1.
END PLOT

BEGIN PLOT /BELLE_2022_I2140379/d01-x02-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^+\eta$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta$
YLabel=$\alpha_{\Sigma^+}\times\alpha_{\Lambda_c\to\Sigma^+\eta}$
YMin=-1.
YMax=1.
END PLOT
BEGIN PLOT /BELLE_2022_I2140379/d01-x02-y02
Title=$\alpha_{\Lambda_c\to\Sigma^+\eta}$ for $\Lambda_c^+\to\Sigma^+\eta$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta$
YLabel=$\alpha_{\Lambda_c\to\Sigma^+\eta}$
YMin=-1.
YMax=1.
END PLOT

BEGIN PLOT /BELLE_2022_I2140379/d01-x03-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^+\eta^\prime$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta^\prime$
YLabel=$\alpha_{\Sigma^+}\times\alpha_{\Lambda_c\to\Sigma^+\eta^\prime}$
YMin=-1.
YMax=1.
END PLOT
BEGIN PLOT /BELLE_2022_I2140379/d01-x03-y02
Title=$\alpha_{\Lambda_c\to\Sigma^+\eta^\prime}$ for $\Lambda_c^+\to\Sigma^+\eta^\prime$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta^\prime$
YLabel=$\alpha_{\Lambda_c\to\Sigma^+\eta^\prime}$
YMin=-1.
YMax=1.
END PLOT

BEGIN PLOT /BELLE_2022_I2140379/d02-x01-y01
Title=$\cos\theta$ for $\Lambda_c^+\to\Sigma^+\pi^0$ and $\bar\Lambda_c^-\to\bar\Sigma^-\pi^0$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2140379/d02-x01-y02
Title=$\cos\theta$ for $\Lambda_c^+\to\Sigma^+\eta$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2140379/d02-x01-y03
Title=$\cos\theta$ for $\Lambda_c^+\to\Sigma^+\eta^\prime$ and $\bar\Lambda_c^-\to\bar\Sigma^-\eta^\prime$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
