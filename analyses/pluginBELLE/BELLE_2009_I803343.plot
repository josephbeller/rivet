BEGIN PLOT /BELLE_2009_I803343/d01-x01-y01
Title=Branching ratio w.r.t $\Lambda\bar{\Lambda}$ mass for $B^0\to\Lambda\bar{\Lambda}K^0$
XLabel=$m_{\Lambda\bar{\Lambda}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{\Lambda\bar{\Lambda}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I803343/d01-x01-y02
Title=Branching ratio w.r.t $\Lambda\bar{\Lambda}$ mass for $B^+\to\Lambda\bar{\Lambda}K^+$
XLabel=$m_{\Lambda\bar{\Lambda}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{\Lambda\bar{\Lambda}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I803343/d01-x01-y03
Title=Branching ratio w.r.t $\Lambda\bar{\Lambda}$ mass for $B^0\to\Lambda\bar{\Lambda}K^{*0}$
XLabel=$m_{\Lambda\bar{\Lambda}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{\Lambda\bar{\Lambda}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I803343/d02-x01-y01
Title=$\Lambda$ helicty angle for $B^+\to\Lambda\bar{\Lambda}K^+$
XLabel=$\cos\theta_p$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_p$ 
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I803343/d03-x01-y01
Title=$\bar\Lambda$  angle for $B^+\to\Lambda\bar{\Lambda}K^+$
XLabel=$\cos\theta_{\bar\Lambda}$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_{\bar\Lambda}$ 
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I803343/d02-x01-y01
Title=Kaon helicty angle for $B^0\to\Lambda\bar{\Lambda}K^{*0}$
XLabel=$\cos\theta_K$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_K$ 
LogY=0
END PLOT
