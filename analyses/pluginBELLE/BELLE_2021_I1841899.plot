BEGIN PLOT /BELLE_2021_I1841899/d01-x01-y01
Title=$\phi\phi$ mass distribution in $B^+\to\phi\phi K^+$
XLabel=$m_{\phi\phi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\phi\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1841899/d01-x01-y02
Title=$K^+\phi$ mass distribution in $B^+\to\phi\phi K^+$
XLabel=$m_{K^+\phi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
