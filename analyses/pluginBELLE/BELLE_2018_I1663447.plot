BEGIN PLOT /BELLE_2018_I1663447/d01-x01-y01
Title=Helicity angle in $B^+\to K^+\eta\gamma$ (using $\eta\to2\gamma$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1663447/d01-x01-y02 (using $\eta\to3\pi$)
Title=Helicity angle in $B^+\to K^+\eta\gamma$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta$
LogY=0

END PLOT

BEGIN PLOT /BELLE_2018_I1663447/d02-x01-y01
Title=$K^+\eta$ mass  distribution in $B^+\to K^+\eta\gamma$ (using $\eta\to2\gamma$)
XLabel=$m_{K^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1663447/d02-x01-y02
Title=$K^+\eta$ mass  distribution in $B^+\to K^+\eta\gamma$ (using $\eta\to3\pi$)
XLabel=$m_{K^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
