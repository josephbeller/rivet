BEGIN PLOT /BESIII_2019_I1722111/d01-x01-y01
Title=$p\phi$ mass distribution in $\psi(2S)\to p\bar{p}\phi$
XLabel=$m_{p\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1722111/d01-x01-y02
Title=$\bar{p}\phi$ mass distribution in $\psi(2S)\to p\bar{p}\phi$
XLabel=$m_{\bar{p}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1722111/d01-x01-y03
Title=$\bar{p}\phi$ mass distribution in $\psi(2S)\to p\bar{p}\phi$
XLabel=$m_{\bar{p}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1722111/dalitz
Title=Dalitz plot for  $\psi(2S)\to p\bar{p}\phi$
XLabel=$m^2_{p\phi}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\phi}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\phi}/{\rm d}m^2_{\bar{p}\phi}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
