BEGIN PLOT /BESIII_2019_I1712729/d01-x01-y01
Title=$\phi\eta^\prime$ mass distribution in $J/\psi\to \phi\eta\eta^\prime$ ($\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\phi\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1712729/d01-x01-y02
Title=$\phi\eta^\prime$ mass distribution in $J/\psi\to \phi\eta\eta^\prime$ ($\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\phi\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1712729/dalitz
Title=Dalitz plot for  $J/\psi\to \phi\eta\eta^\prime$
XLabel=$m^2_{p\phi}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\phi}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\phi}/{\rm d}m^2_{\bar{p}\phi}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
