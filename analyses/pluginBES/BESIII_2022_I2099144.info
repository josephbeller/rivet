Name: BESIII_2022_I2099144
Year: 2022
Summary: Analysis of $\psi(2S)$ decays to $\Xi^-\bar{\Xi}^+$
Experiment: BESIII
Collider: BEPC
InspireID: 2099144
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2206.10900 [hep-ex]
RunInfo: e+e- > psi(2S) 
NeedCrossSection: no
Beams: [e-, e+]
Energies: [3.686]
Description:
  'Analysis of the angular distribution of the baryons, and decay products, produced in
   $e^+e^-\to \psi(2S) \to \Xi^-\bar{\Xi}^+$.
   Gives information about the decay and is useful for testing correlations in hadron decays. N.B. the data is not corrected and should only be used qualatively.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022lsz
BibTeX: '@article{BESIII:2022lsz,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Observation of $\Xi^{-}$ Hyperon Transverse Polarization in $\psi(3686)\rightarrow\Xi^{-}\bar\Xi^{+}$}",
    eprint = "2206.10900",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "6",
    year = "2022"
}
'
