BEGIN PLOT /BESIII_2015_I1386254
LogY=0
END PLOT

BEGIN PLOT /BESIII_2015_I1386254/d01-x01-y01
Title=$\pi^+\pi^-\pi^0$ mass in $D^+\to \pi^+\pi^-\pi^0 e^+\nu_e$
XLabel=$m_{\pi^+\pi^-\pi^0}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2015_I1386254/d01-x01-y02
Title=$e^+\nu_e$ mass squared in $D^+\to \pi^+\pi^-\pi^0 e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2015_I1386254/d01-x01-y03
Title=$\cos\theta_e$ in $D^+\to \pi^+\pi^-\pi^0 e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BESIII_2015_I1386254/d01-x01-y04
Title=$\cos\theta_K$ in $D^+\to \pi^+\pi^-\pi^0 e^+\nu_e$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT

BEGIN PLOT /BESIII_2015_I1386254/d01-x01-y05
Title=$\chi$ in $D^+\to \pi^+\pi^-\pi^0 e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
