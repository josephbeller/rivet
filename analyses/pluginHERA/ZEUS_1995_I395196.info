Name: ZEUS_1995_I395196
Year: 1995
Summary: Neutral strange particle production in deep inelastic scattering at HERA (ZEUS)
Experiment: ZEUS
Collider: HERA
InspireID: 395196
Status: VALIDATED
Reentrant: True
Authors:
 - Can Sueslue <can.suslu@ug.bilkent.edu.tr>
 - Hannes Jung <hannes.jung@desy.de>
References:
 - Z. Phys.C 68( 1995) 29
 - doi:10.1007/BF01579802
 - arXiv:hep-ex/9505011
RunInfo: Cuts are $10<Q2<640 GeV^2$, $0.0003<x_{bj}<0.01$,and $y>0.04$. For Kaons, $1.3< \eta <1.3$ and $0.5< p_t<4.0 GeV$. For Lambdas $1.3< \eta <1.3 $ and $0.5< p_t<3.5 GeV$.
Beams: [[e-, p+],[p+,e-]]
Energies: [[26.7,820],[820,26.7]]
Description:
  'Measurements of  $K^0$ and $\Lambda$ production in neutral current, deep inelastic scattering of 26.7 GeV electrons and 820 GeV protons in the kinematic range $10<Q^2<640$,  $0.0003<x<0.01$, and $y>0.04$. Average multiplicities for $K^0$ and $\Lambda$ production are determined for transverse momenta $p_T >0.5> $ GeV and pseudorapidities $|\eta| < 1.3 $. The production properties of $K^0$  in events with and without a large rapidity gap with respect to the proton direction are compared. The ratio of neutral $K^0$ to charged particles per event in the measured kinematic range is, within the present statistics, the same in both samples.'
ValidationInfo:
  'Particles are selected by their MC ids. Their properties are obtained by DISKinematics. For rapidity gap events, DISRapidityGap is used.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: ZEUS:1995how
BibTeX: '@article{ZEUS:1995how,
    author = "Derrick, M. and others",
    collaboration = "ZEUS",
    title = "{Neutral strange particle production in deep inelastic scattering at HERA}",
    eprint = "hep-ex/9505011",
    archivePrefix = "arXiv",
    reportNumber = "DESY-95-084",
    doi = "10.1007/BF01579802",
    journal = "Z. Phys. C",
    volume = "68",
    pages = "29",
    year = "1995"
}
'
