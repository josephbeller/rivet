Name: CLEO_2012_I1086166
Year: 2012
Summary: Mass distributions in the decay $D^0\to K^+K^-\pi^+\pi^-$
Experiment: CLEO
Collider: CESR
InspireID: 1086166
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 85 (2012) 122002
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the mass distributions in the decay $D^0\to K^+K^-\pi^+\pi^-$ by CLEO. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2012beo
BibTeX: '@article{CLEO:2012beo,
    author = "Artuso, M. and others",
    collaboration = "CLEO",
    title = "{Amplitude analysis of $D^0\to K^+K^-\pi^+\pi^-$}",
    eprint = "1201.5716",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CLNS-11-2082, CLEO-11-08",
    doi = "10.1103/PhysRevD.85.122002",
    journal = "Phys. Rev. D",
    volume = "85",
    pages = "122002",
    year = "2012"
}
'
