BEGIN PLOT /CLEO_1986_I220652/d01-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma$
LogY=0
END PLOT
