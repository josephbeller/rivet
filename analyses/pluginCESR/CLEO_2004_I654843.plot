BEGIN PLOT /CLEO_2004_I654843/d01-x01-y01
Title=$D^0\to K^- e^+ \nu_e$
XLabel=$q^2/$GeV$^2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2004_I654843/d01-x01-y02
Title=$D^0\to \pi^- e^+ \nu_e$
XLabel=$q^2/$GeV$^2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
