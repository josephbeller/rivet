BEGIN PLOT /VENUS_1995_I392360
XLabel=$|\cos\theta|$
YLabel=$\mathrm{d}\sigma(\gamma\gamma\to \pi^+\pi^-)/\mathrm{d}|\cos\theta|$ [nb]
LogY=0
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.00<\sqrt{s}<1.05$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.05<\sqrt{s}<1.10$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.10<\sqrt{s}<1.15$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.15<\sqrt{s}<1.20$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d06-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.20<\sqrt{s}<1.25$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d07-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.25<\sqrt{s}<1.30$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d08-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.30<\sqrt{s}<1.35$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d09-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.35<\sqrt{s}<1.40$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d10-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.40<\sqrt{s}<1.45$ GeV
END PLOT
BEGIN PLOT /VENUS_1995_I392360/d11-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.45<\sqrt{s}<1.50$ GeV
END PLOT
