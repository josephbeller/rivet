BEGIN PLOT /BABAR_2018_I1667191/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\Upsilon(1S)\to\gamma\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d02-x01-y01
Title=$K^+K^-$ mass distribution in $\Upsilon(1S)\to\gamma K^+K^-$
XLabel=$m_{K^+K^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d03-x01-y01
Title=$\pi^+$ helicty angle in $\Upsilon(2,3S)\to\pi^+\pi^-\Upsilon(1S)$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d04-x01-y01
Title=$\gamma$ helicty angle in $\Upsilon(1S)\to\gamma\pi^+\pi^-$ ($0.6<m_{\pi^+\pi^-}<1$\,GeV)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d04-x01-y02
Title=$\pi^+$ helicty angle in $\Upsilon(1S)\to\gamma\pi^+\pi^-$ ($0.6<m_{\pi^+\pi^-}<1$\,GeV)
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2018_I1667191/d05-x01-y01
Title=$\gamma$ helicty angle in $\Upsilon(1S)\to\gamma\pi^+\pi^-$ ($1.092<m_{\pi^+\pi^-}<1.46$\,GeV)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d05-x01-y02
Title=$\pi^+$ helicty angle in $\Upsilon(1S)\to\gamma\pi^+\pi^-$ ($1.092<m_{\pi^+\pi^-}<1.46$\,GeV)
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2018_I1667191/d06-x01-y01
Title=$\gamma$ helicty angle in $\Upsilon(1S)\to\gamma K^+K^-$ ($1.424<m_{K^+K^-}<1.62$\,GeV)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1667191/d06-x01-y02
Title=$K^+$ helicty angle in $\Upsilon(1S)\to\gamma K^+K^-$ ($1.424<m_{K^+K^-}<1.62$\,GeV)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_K$
LogY=0
END PLOT
