Name: BABAR_2012_I1125567
Year: 2012
Summary: Mass distributions in the decay $\tau^-\to\pi^-K^0_SK^0_S$
Experiment: BABAR
Collider: PEP-II
InspireID: 1125567
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 86 (2012) 092013
RunInfo: Any process producing tau leptons, originally e+e-
Description:
'Measurements of mass distributions in $\tau^-\to\pi^-K^0_SK^0_S$ decays.
 The data were read from the plots in the paper and are not corrected, although the backgrounds given in the paper have been subtracted. The plots should therefore only be used for qualitative comparisons however the data is useful as there are not corrected distributions for this decay mode.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2012nue
BibTeX: '@article{BaBar:2012nue,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{The branching fraction of $\tau^- \to \pi^- K^0_S K^0_S (\pi^0) \nu_\tau$ decays}",
    eprint = "1208.0376",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BABAR-PUB-12-021, SLAC-PUB-15206",
    doi = "10.1103/PhysRevD.86.092013",
    journal = "Phys. Rev. D",
    volume = "86",
    pages = "092013",
    year = "2012"
}
'
