BEGIN PLOT /BABAR_2010_I853279/d01-x01-y01
Title=$K^0_S\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/d01-x01-y02
Title=$K^0_S\pi^+$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/dalitz1
Title=Dalitz plot for $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^-}/{\rm d}m^2_{K^0_S\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT


BEGIN PLOT /BABAR_2010_I853279/d01-x01-y04
Title=$K^0_SK^-$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/d01-x01-y05
Title=$K^0_SK^+$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/d01-x01-y06
Title=$K^+K^-$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I853279/dalitz2
Title=Dalitz plot for $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_SK^+}/{\rm d}m^2_{K^+K^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
