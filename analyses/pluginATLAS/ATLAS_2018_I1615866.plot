BEGIN PLOT /ATLAS_2018_I1615866/d01-x01-y01
Title=Fiducial Cross Section for $\mu^+\mu^-$ production
YLabel=$\sigma$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2018_I1615866/d02-x01-y01
Title=Fiducial Differential Cross Section as a function of the $\mu^+\mu^-$ mass
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+\mu^-}$ [pb/GeV]
XLabel=$m_{\mu^+\mu^-}$ [GeV]
LogY=0
END PLOT
