# BEGIN PLOT /ATLAS_2019_I1720438/d..
LogY=0
RatioPlot=1
LegendXPos=0.05
LegendYPos=0.5
YTwosidedTicks=1
Title=last bin includes overflow
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d20-x01-y01
Title=
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$N_\mathrm{jets}$
LogY=1
XCustomMajorTicks=0 0 1 1 2 2 3 3 4 4 5 $\geq5$
XMinorTickMarks = 0
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d08-x01-y01
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$p_\mathrm{T}^Z$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d10-x01-y01
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$p_\mathrm{T}^W$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d12-x01-y01
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$m_\mathrm{T}^{WZ}$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d14-x01-y01
Title=
YLabel=$\Delta\sigma^{\mathrm{fid.}}$
XLabel=$|\Delta \phi_{WZ}|$
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d16-x01-y01
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$p_\mathrm{T}^\nu$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d18-x01-y01
Title=
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$|y_Z-y_{l,W}|$ 
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1720438/d22-x01-y01
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ 
XLabel=$m_{jj}$ [GeV]
LogY=1
# END PLOT

